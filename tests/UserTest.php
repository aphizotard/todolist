<?php

namespace App\Tests;

use App\Entity\Todolist;
use App\Entity\User;
use App\Service\EmailService;
use PHPUnit\Framework\TestCase;
use Swift_Mailer;
use Symfony\Component\Mailer\MailerInterface;


class UserTest extends TestCase
{
    private $user;
    private $todolist;
    private $newUser;

    public function __construct()
    {
        parent::__construct();
        $this->todolist = (new Todolist())
            ->setName('Nom de la todolist');

        $this->user = (new User())
            ->setLastname('Phiz')
            ->setFirstname('Alex')
            ->setEmail('alexisphiz@gmail.com')
            ->setPassword('passwordd')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')))
            ->addTodolist($this->todolist);


        $this->newUser = (new User())
            ->setLastname('Famille')
            ->setFirstname('Aymeric')
            ->setEmail('alexisphiz@gmail.com')
            ->setPassword('passwordd')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));
    }

    public function testIsValid()
    {
        $this->assertEquals(true, $this->user->isValid());
    }

    public function testIsValidFirstname()
    {
        $this->user->setFirstname('');
        $this->assertEquals(false, $this->user->isValidFirstname());
    }

    public function testIsValidLastname()
    {
        $this->user->setLastname('');
        $this->assertEquals(false, $this->user->isValidLastname());
    }

    public function testIsValidEmail()
    {
        $this->user->setEmail('dajflkdjfkdj');
        $this->assertEquals(false, $this->user->isValidEmail());
    }


    public function testIsValidPasswordLess()
    {
        $this->user->setPassword('pass');
        $this->assertEquals(false, $this->user->isValidPassword());
    }

    public function testIsValidPasswordMore()
    {
        $this->user->setPassword('passWpassWpassWpassWpassWpassWpassWpassWpassWpassWpassW');
        $this->assertEquals(false, $this->user->isValidPassword());
    }

    public function testIsValidPasswordGood()
    {
        $this->user->setPassword('passwordDeTest');
        $this->assertEquals(true, $this->user->isValidPassword());
    }

    public function testIsValidBirthday()
    {
        $this->user->setBirthday((new \DateTime())->sub(new \DateInterval('P10Y')));
        $this->expectExceptionMessage('Age minimum : 13 ans');
        $this->assertEquals(false, $this->user->isValidBirthday());
    }

    public function testCanAddTodolist()
    {
        $this->assertEquals(false, $this->user->canAddTodolist());
    }

    //fonction basique d'ajout verifier avec le get
    public function testAddTodolist()
    {
        //suppression de la todolist du constructeur
        $this->user->removeTodolist($this->todolist);
        $this->user->addTodolist($this->todolist);
        $this->assertCount(1, $this->user->getTodolist());
    }

    // fonction qui test l'ajout d'au dessus a un user defini
    public function testAddTodolistToUseraccount()
    {
        $this->expectExceptionMessage("Erreur: Un utilisateur peut n’avoir qu’une seule
ToDoList");
        $this->assertEquals(false, $this->todolist->addTodolistToUseraccount($this->user,$this->todolist));
    }

    // le newUser n'a pas de todoliste
    public function testAddTodolistToUseraccountWithZeroTodo()
    {
        $this->assertEquals(true, $this->todolist->addTodolistToUseraccount($this->newUser,$this->todolist));
    }


    public function testSendEmail()
    {
        $mailer = $this->createMock(Swift_Mailer::class);
        $emailMessage = new EmailService($mailer);
        $this->assertEquals(true, $emailMessage->sendEmail($this->user));

    }

    public function testSendEmailUserMineur()
    {
        $this->user->setBirthday((new \DateTime())->sub(new \DateInterval('P10Y')));
        $mailer = $this->createMock(Swift_Mailer::class);
        $emailMessage = new EmailService($mailer);
        $this->assertEquals(false, $emailMessage->sendEmail($this->user));

    }


}
