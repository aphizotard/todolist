<?php

namespace App\Tests;

use App\Entity\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    private $item;
    private $item2;

    public function __construct()
    {
        parent::__construct();
        $this->item = (new Item())
            ->setName('Controle')
            ->setContent('Lorem Ipsum')
            ->setCreatedAt(new \DateTime());

        $this->item2 = (new Item())
            ->setName('Rendu maison')
            ->setContent('Lorem Ipsum')
            ->setCreatedAt(new \DateTime());
    }

    public function testIsValidName()
    {
        $this->item->setName('');
        $this->assertEquals(false, $this->item->isValidName());
    }

    public function testIsValidNameUnique()
    {
        $this->item2->setName('Controle');
        $this->assertEquals(false, $this->item->isValidNameUnique($this->item, $this->item2));
    }


    public function testIsValidContentMore()
    {
        $this->item->setContent('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset It was popularised in the 1960s with the release of LetrasetIt was popularised in the 1960s with the release of Letraset');
        $this->assertEquals(false, $this->item->isValidContent());
    }

    public function testIsValidContentGood()
    {
        $this->item->setContent('Lorem Ipsum');
        $this->assertEquals(true, $this->item->isValidContent());
    }

    public function testIsNotEmptyContent()
    {
        $this->item->setContent('');
        $this->assertEquals(false, $this->item->isNotEmptyContent());
    }

}
