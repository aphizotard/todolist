<?php

namespace App\Tests;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class TodolistTest extends TestCase
{
    private $todolist;
    private $item;

    public function __construct()
    {
        parent::__construct();

        $this->todolist = (new Todolist())
            ->setName('Ma todolist');

        // creation de 1 item, et avoir la variable pour le function de test
        $this->item = (new Item())
            ->setName('lorem')
            ->setContent('loremipsum')
            ->setCreatedAt(new \DateTime());
        $this->todolist->addItem($this->item);

        //creation de 9 item pour obtenir le nombre de max pour les functions tests
        for ($i = 1; $i < 10; $i++) {
            $item = (new Item())
                ->setName('lorem')
                ->setContent('loremipsum')
                ->setCreatedAt(new \DateTime());
            $this->todolist->addItem($item);
        }
    }

    public function testAddItemCount()
    {
        $this->todolist->addItem($this->item);
        $this->assertCount(10, $this->todolist->getItems());
    }

    public function testCanAddItem()
    {
        $this->assertEquals(false, $this->todolist->canAddItem());
    }
}
