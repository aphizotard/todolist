<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function principale(){
        $user = new User();
        $user->setFirstname('Alexis');
        $user->setLastname('Phizotard');
        $user->setEmail('alexisphiz@gmail.com');
        $user->setPassword('password');
        $user->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));
    }
}