<?php
namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mime\Email;
use Exception;

class EmailService {


    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail(User $user){
        $email = (new \Swift_Message())
            ->setFrom('noreply@example.com')
            ->setTo('user@example.com')
            ->setReplyTo($user->getEmail())
            ->setBody('Tu as ajouté blalabalbalb');

        if(($user->getBirthday()->diff(new \DateTime())->format('%y')) > 18){
            $this->mailer->send($email);
            return true;
        }else{
            return false;
        }

    }

}