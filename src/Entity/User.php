<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="account")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 8,
     *      max = 40,
     *      minMessage = "Mot de passe trop court",
     *      maxMessage = "Mot de passe trop long"
     * )
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Todolist", mappedBy="usersaccount", cascade={"persist", "remove"})
     */
    private $todolist;

    public function __construct()
    {
        $this->todolist = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Todolist[]
     */
    public function getTodolist(): Collection
    {
        return $this->todolist;
    }

    public function addTodolist(Todolist $todolist): self
    {
        if ($this->canAddTodolist() && !$this->todolist->contains($todolist)) {
            $this->todolist[] = $todolist;
            $todolist->setUsersaccount($this);
        }

        return $this;
    }

    public function removeTodolist(Todolist $todolist): self
    {
        if ($this->todolist->contains($todolist)) {
            $this->todolist->removeElement($todolist);
            // set the owning side to null (unless already changed)
            if ($todolist->getUsersaccount() === $this) {
                $todolist->setUsersaccount(null);
            }
        }

        return $this;
    }

    public function isValidFirstname(): bool
    {
        if (empty($this->firstname)) {
            return false;
        }
        return true;
    }

    public function isValidLastname(): bool
    {
        if (empty($this->lastname)) {
            return false;
        }
        return true;
    }

    public function isValidEmail(): bool
    {
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    public function isValidPassword(): bool
    {
        if (strlen($this->password) < 8 || strlen($this->password) > 40) {
            return false;
        }
        return true;
    }

    public function isValidBirthday()
    {
        if ((new \DateTime())->diff($this->birthday, true)->y < 13)
            throw new \Exception('Age minimum : 13 ans');

        return true;
    }

    public function isValid(): bool
    {
        $success = true;
        if (!$this->isValidLastname()) {
            $success = false;
        }
        if (!$this->isValidFirstname()) {
            $success = false;
        }
        if (!$this->isValidEmail()) {
            $success = false;
        }
        if (!$this->isValidPassword()) {
            $success = false;
        }
        if (!$this->isValidBirthday()) {
            $success = false;
        }
        return $success;
    }

    public function canAddTodolist()
    {
        if (count($this->todolist) > 0) {
            return false;
        }
        return true;
    }

}
