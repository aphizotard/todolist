<?php

namespace App\Entity;

use App\Service\EmailService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodolistRepository")
 */
class Todolist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="todolist")
     */
    private $usersaccount;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Item", mappedBy="todolists")
     * @Assert\Count(
     *      max = 10,
     *      maxMessage = "Erreur : Une ToDoList peut contenir de 0 à 10 items"
     * )
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsersaccount(): ?User
    {
        return $this->usersaccount;
    }

    public function setUsersaccount(?User $usersaccount): self
    {
        $this->usersaccount = $usersaccount;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if ($this->canAddItem() && !$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setTodolists($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    public function canAddItem(): bool
    {
        if (count($this->items) >= 10) {
            return false;
        }
        return true;
    }

    public function addTodolistToUseraccount(User $user, Todolist $todolist)
    {
        if (!$user->canAddTodolist()) {
            throw new Exception("Erreur: Un utilisateur peut n’avoir qu’une seule
ToDoList");
        }
        $todolist->setUsersaccount($user);
        $user->addTodolist($todolist);
        return true;
    }


    public function addItemToTodolist(Item $newItem, Todolist $todolist)
    {
        // gérer les 30 minutes items
    }

}
