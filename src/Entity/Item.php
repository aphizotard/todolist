<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 * @UniqueEntity("name")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Todolist", inversedBy="items")
     */
    private $todolists;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min="1", max="1000")
     */
    private $content;

    public function __construct()
    {
        $this->todolists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Todolist[]
     */
    public function getTodolists(): Collection
    {
        return $this->todolists;
    }

    public function setTodolists(Todolist $todolists)
    {
        $this->todolists = $todolists;

        return $this;
    }

    public function addTodolist(Todolist $todolists): self
    {
        if (!$this->todolists->contains($todolists)) {
            $this->todolists[] = $todolists;
            $todolists->addItem($this);
        }

        return $this;
    }

    public function removeTodolist(Todolist $todolist): self
    {
        if ($this->todolists->contains($todolist)) {
            $this->todolists->removeElement($todolist);
            $todolist->removeItem($this);
        }

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function isValidName(): bool
    {
        if (empty($this->name)) {
            return false;
        }
        return true;
    }

    public function isValidNameUnique(Item $item1, Item $item2){
        if($item1->getName() === $item2->getName()){
            return false;
        }
        return true;
    }

    public function isNotEmptyContent(): bool
    {
        if (empty($this->content)) {
            return false;
        }
        return true;
    }

    public function isValidContent(): bool
    {
        if (strlen($this->content) > 1000) {
            return false;
        }
        return true;
    }

}
